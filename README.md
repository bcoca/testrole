Role Name
=========

This is an Ansible test role

Requirements
------------

Ansible

Role Variables
--------------

Posisbly

Dependencies
------------

don't know yet

Example Playbook
----------------

    - hosts: servers
      tasks:
		- import_role: name=testrole

License
-------

GPL3+

Author Information
------------------

Me
